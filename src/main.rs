use std::{time::Duration, fs::File, env, process::exit};

use markov::Chain;
use megalodon::megalodon::PostStatusInputOptions;
use tokio::time;

const POST_INTERVAL: u64 = 60*10;
//const POST_INTERVAL: u64 = 5;

#[tokio::main]
async fn main() {
    simple_logger::init().unwrap();
    let Ok(token) = env::var("WORDS_OF_GOD_TOKEN") else {
        log::error!("Environment variable $WORDS_OF_GOD_TOKEN isn't set, no auth token available.");
        exit(1);
    };

    let client = megalodon::generator(
        megalodon::SNS::Pleroma, 
        String::from("https://fedi.absturztau.be"), 
        Some(token), 
        None
    );

    let mut god = God::new(Some(1));

    log::info!("God is feeding...");
    god.feed_file("/etc/wordsofgod-bot/bibles/asv.txt");
    god.feed_file("/etc/wordsofgod-bot/bibles/cpdv.txt");
    god.feed_file("/etc/wordsofgod-bot/bibles/dbt.txt");
    god.feed_file("/etc/wordsofgod-bot/bibles/drb.txt");
    god.feed_file("/etc/wordsofgod-bot/bibles/erv.txt");
    god.feed_file("/etc/wordsofgod-bot/bibles/jps.txt");
    god.feed_file("/etc/wordsofgod-bot/bibles/slt.txt");
    god.feed_file("/etc/wordsofgod-bot/bibles/wbt.txt");
    god.feed_file("/etc/wordsofgod-bot/bibles/ylt.txt");
    god.feed_file("/etc/wordsofgod-bot/bibles/kjv.txt");
    for _ in 0..10 {
        god.feed_file("/etc/wordsofgod-bot/bibles/1984 - George Orwell_djvu.txt");
    }    
    log::info!("God finished, starting...");
    
    let postcfg = PostStatusInputOptions {
        spoiler_text: Some("markov generated bible verse".to_string()),
        visibility: Some(megalodon::entities::StatusVisibility::Unlisted),
        media_ids: None,
        poll: None,
        in_reply_to_id: None,
        sensitive: None,
        scheduled_at: None,
        language: None,
        quote_id: None,
    };

    let mut interval = time::interval(Duration::from_secs(POST_INTERVAL));
    interval.tick().await;

    loop {
        let line = god.speak();
        log::info!("L: {}",line.clone());
        let res = client.post_status(
            line.clone(), 
            Some(&postcfg)
        ).await;

        if let Err(e) = res {
            log::error!("Error: {:?}", e);
        } else {
            log::info!("Posted line: {}", line);
        }

        interval.tick().await;
    }


}

struct God {
    chain: Chain<String>
}

impl God {
    pub fn new(order: Option<usize>) -> God {
        if let Some(order) = order {
            God {
                chain: Chain::of_order(order)
            }
        } else {
            God {
                chain: Chain::new()
            }
        }
    }

    pub fn feed_file(&mut self, path: &str) {
        self.chain.feed_file(path)
            .expect(&format!("Path '{:?}'", path));
    }

    pub fn feed_str(&mut self, s: &str) {
        let lines: Vec<&str> = s.lines().collect();
        for l in lines {
            self.chain.feed_str(s);
        }
    }

    pub fn speak(&self) -> String {
        self.chain.generate_str()
    }
}
