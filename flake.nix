{
  inputs = {
    naersk.url = "github:nix-community/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
      in
      {
        packages = {
          default = naersk-lib.buildPackage {
            src = ./.;
            buildInputs = with pkgs; [ pkg-config openssl ];
          };
        };
        nixosModules.wordsofgod = { pkgs, ... }: {
          systemd.services.wordsofgod-bot = {
            wantedBy = [ "multi-user.target" ];
            serviceConfig.ExecStart = "${self.packages.default}/bin/wordsofgod";
            serviceConfig.EnvironmentFile = "/etc/wordsofgod-bot/wordsofgod.env";
          };
        };

        devShell = with pkgs; mkShell {
          buildInputs = [ cargo rustc rustfmt pre-commit rustPackages.clippy rust-analyzer  ];
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
        };
      }
    );
}
